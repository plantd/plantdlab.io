<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [@fortawesome/fontawesome-svg-core - SVG with JavaScript version](#fortawesomefontawesome-svg-core---svg-with-javascript-version)
  - [Installation](#installation)
  - [Documentation](#documentation)
  - [Issues and support](#issues-and-support)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# @fortawesome/fontawesome-svg-core - SVG with JavaScript version

> "I came here to chew bubblegum and install Font Awesome 5 - and I'm all out of bubblegum"

[![npm](https://img.shields.io/npm/v/@fortawesome/fontawesome-svg-core.svg?style=flat-square)](https://www.npmjs.com/package/@fortawesome/fontawesome-svg-core)

## Installation

```
$ npm i --save @fortawesome/fontawesome-svg-core
```

Or

```
$ yarn add @fortawesome/fontawesome-svg-core
```

## Documentation

Get started [here](https://fontawesome.com/how-to-use/on-the-web/setup/getting-started). Continue your journey [here](https://fontawesome.com/how-to-use/on-the-web/advanced).

Or go straight to the [API documentation](https://fontawesome.com/how-to-use/with-the-api).

## Issues and support

Start with [GitHub issues](https://github.com/FortAwesome/Font-Awesome/issues) and ping us on [Twitter](https://twitter.com/fontawesome) if you need to.
