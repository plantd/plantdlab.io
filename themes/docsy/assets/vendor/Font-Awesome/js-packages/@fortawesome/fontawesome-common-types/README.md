<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [@fortawesome/fontawesome-common-types - SVG with JavaScript](#fortawesomefontawesome-common-types---svg-with-javascript)
  - [What is this package?](#what-is-this-package)
  - [Here be dragons](#here-be-dragons)
  - [Issues and support](#issues-and-support)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# @fortawesome/fontawesome-common-types - SVG with JavaScript

> "I came here to chew bubblegum and install Font Awesome 5 - and I'm all out of bubblegum"

[![npm](https://img.shields.io/npm/v/@fortawesome/fontawesome-common-types.svg?style=flat-square)](https://www.npmjs.com/package/@fortawesome/fontawesome-common-types)

## What is this package?

Font Awesome 5 JavaScript packages support TypeScript. This package abstracts out some of the common definitions that those packages use.

## Here be dragons

If you are trying to import types from this package we *highly* recommend you do the following instead as *all types in this package are re-exported to the main fontawesome package*.

your.ts

```
import {
  IconName
} from `@fortawesome/fontawesome`

const myIcon: IconName = "..."
```

## Issues and support

Start with [GitHub issues](https://github.com/FortAwesome/Font-Awesome/issues) and ping us on [Twitter](https://twitter.com/fontawesome) if you need to.
