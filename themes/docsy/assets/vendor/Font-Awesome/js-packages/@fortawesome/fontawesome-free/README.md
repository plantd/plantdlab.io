<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [@fortawesome/fontawesome-free - The Official Font Awesome 5 NPM package](#fortawesomefontawesome-free---the-official-font-awesome-5-npm-package)
  - [Installation](#installation)
  - [What's included?](#whats-included)
  - [Documentation](#documentation)
  - [Issues and support](#issues-and-support)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# @fortawesome/fontawesome-free - The Official Font Awesome 5 NPM package

> "I came here to chew bubblegum and install Font Awesome 5 - and I'm all out of bubblegum"

[![npm](https://img.shields.io/npm/v/@fortawesome/fontawesome-free.svg?style=flat-square)](https://www.npmjs.com/package/@fortawesome/fontawesome-free)

## Installation

```
$ npm i --save @fortawesome/fontawesome-free
```

Or

```
$ yarn add @fortawesome/fontawesome-free
```

## What's included?

**This package includes all the same files available through our Free and Pro CDN.**

* /js - All JavaScript files associated with Font Awesome 5 SVG with JS
* /css - All CSS using the classic Web Fonts with CSS implementation
* /sprites - SVG icons packaged in a convenient sprite
* /scss, /less - CSS Pre-processor files for Web Fonts with CSS
* /webfonts - Accompanying files for Web Fonts with CSS
* /svg - Individual icon files in SVG format

## Documentation

Get started [here](https://fontawesome.com/get-started). Continue your journey [here](https://fontawesome.com/how-to-use).

Or go straight to the [API documentation](https://fontawesome.com/how-to-use/with-the-api).

## Issues and support

Start with [GitHub issues](https://github.com/FortAwesome/Font-Awesome/issues) and ping us on [Twitter](https://twitter.com/fontawesome) if you need to.
