<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [General outline](#general-outline)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---
layout: docs
title: Best practices
description: Learn about some of the best practices we've gathered from years of working on and using Bootstrap.
group: getting-started
---

We've designed and developed Bootstrap to work in a number of environments. Here are some of the best practices we've gathered from years of working on and using it ourselves.

{% capture callout %}
**Heads up!** This copy is a work in progress.
{% endcapture %}
{% include callout.html content=callout type="info" %}

### General outline

- Working with CSS
- Working with Sass files
- Building new CSS components
- Working with flexbox
- Ask in [Slack](https://bootstrap-slack.herokuapp.com/)
