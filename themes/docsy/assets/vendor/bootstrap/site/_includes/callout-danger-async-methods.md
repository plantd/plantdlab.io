<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Asynchronous methods and transitions](#asynchronous-methods-and-transitions)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

{% capture callout %}
#### Asynchronous methods and transitions

All API methods are **asynchronous** and start a **transition**. They return to the caller as soon as the transition is started but **before it ends**. In addition, a method call on a **transitioning component will be ignored**.

[See our JavaScript documentation for more information.]({{ site.baseurl }}/docs/{{ site.docs_version }}/getting-started/javascript/)
{% endcapture %}
{% include callout.html content=callout type="danger" %}
