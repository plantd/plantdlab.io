<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Bug reports](#bug-reports)
- [How-to](#how-to)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Bug reports

See the [contributing guidelines](CONTRIBUTING.md) for sharing bug reports.

### How-to

For general troubleshooting or help getting started:

- Join [the official Slack room](https://bootstrap-slack.herokuapp.com/).
- Chat with fellow Bootstrappers in IRC. On the `irc.freenode.net` server, in the `##bootstrap` channel.
- Ask and explore Stack Overflow with the [`bootstrap-4`](https://stackoverflow.com/questions/tagged/bootstrap-4) tag.
